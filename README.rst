Pure Flix Kodi Plugin
=====================================

This Kodi plugin is not endorsed by PureFlix. Please don't contact them for support.

This plug in is based off the right now media kodi plugin

https://github.com/cdjc/rnm


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Features:

None currently being modified to be converted to pureflix
